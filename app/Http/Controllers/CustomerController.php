<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Builder;

class CustomerController extends Controller
{
    public function selectAllCustomers() {


        try {
            $customers = Customers::get();

            $status["code"] = 200;
            $status["response"] = "success";
            $status["message"] = "Success fetching data";
            $response["status"] = $status;
            $response["result"] = $customers;

            return response()->json($response, $status["code"]);

        } catch(Excption $ex) {

            $status["code"] = 500;
            $status["response"] = "failed";
            $status["message"] = "Failed to fetching data";
            $response["status"] = $status;
            $response["result"] = null;

            return response()->json($response, $status["code"]);
        }

    }

    public function addCustomers(Request $req) {

        try {

            $customer = new Customers;
            $customer->name = $req->name;
            $customer->email = $req->email;
            $customer->password = Hash::make($req->password);
            $customer->gender = $req->gender;
            $customer->is_married = $req->is_married;
            if($customer->save())
            {
                $status["code"] = 200;
                $status["response"] = "success";
                $status["message"] = "Success fetching data";
            }
            else
            {
                $status["code"] = 500;
                $status["response"] = "failed";
                $status["message"] = "Failed to insert data";
            }

            $response["status"] = $status;
            $response["result"] = null;
            
            return response()->json($response, $status["code"]);

        } catch(Exception $ex) {
            $status["code"] = 500;
            $status["response"] = "failed";
            $status["message"] = "Failed to insert data";
            $response["status"] = $status;
            $response["result"] = null;

            return response()->json($response, $status["code"]);
        }
    }

    public function getCustomersById($id = 0) {

        try {

            if(Customers::where("id", $id)->exists()) {
                $customers = Customers::where("id", $id)->get();

                $status["code"] = 200;
                $status["response"] = "success";
                $status["message"] = "Success fetching data";

            } else {

                $status["code"] = 404;
                $status["response"] = "failed";
                $status["message"] = "Data not found";
                $customers = null;

            }

            $response["status"] = $status;
            $response["result"] = $customers;

            return response()->json($response, $status["code"]);

        } catch(Exception $ex) {

            $status["code"] = 500;
            $status["response"] = "failed";
            $status["message"] = "Failed to fetch data";
            $response["status"] = $status;
            $response["result"] = null;

            return response()->json($response, $status["code"]);

        }

    }

    public function updateCustomers(Request $req, $id = 0) {

        try {

            if(Customers::where("id", $id)->exists()) {

                $customer = Customers::find($id);
                $customer->name = $req->name;
                $customer->email = $req->email;
                if($req->password != ""){
                $customer->password = Hash::make($req->password);
                }
                $customer->gender = $req->gender;
                $customer->is_married = $req->is_married;
                if($customer->save())
                {
                    $status["code"] = 200;
                    $status["response"] = "success";
                    $status["message"] = "Success update data";
                }
                else
                {
                    $status["code"] = 500;
                    $status["response"] = "failed";
                    $status["message"] = "Failed to update data";
                }

            } else {

                $status["code"] = 404;
                $status["response"] = "failed";
                $status["message"] = "Data not found";

            }

            $response["status"] = $status;
            $response["result"] = null;
            
            return response()->json($response, $status["code"]);

        } catch(Exception $ex) {
            $status["code"] = 500;
            $status["response"] = "failed";
            $status["message"] = "Failed to update data ".$ex->getMessage();
            $response["status"] = $status;
            $response["result"] = null;

            return response()->json($response, $status["code"]);
        }
    }

    public function deleteCustomers($id = 0) {

        try {

            if(Customers::where("id", $id)->exists()) {

                if(Customers::where('id', $id)->delete())
                {
                    $status["code"] = 200;
                    $status["response"] = "success";
                    $status["message"] = "Success delete data";
                }
                else
                {
                    $status["code"] = 500;
                    $status["response"] = "failed";
                    $status["message"] = "Failed to delete data";
                }

            } else {

                $status["code"] = 404;
                $status["response"] = "failed";
                $status["message"] = "Data not found";

            }

            $response["status"] = $status;
            $response["result"] = null;
            
            return response()->json($response, $status["code"]);

        } catch(Exception $ex) {
            $status["code"] = 500;
            $status["response"] = "failed";
            $status["message"] = "Failed to delete data ".$ex->getMessage();
            $response["status"] = $status;
            $response["result"] = null;

            return response()->json($response, $status["code"]);
        }
    }
}
