<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('customers', 'App\Http\Controllers\CustomerController@selectAllCustomers');
Route::get('customers/{id}', 'App\Http\Controllers\CustomerController@getCustomersById');
Route::post('customers', 'App\Http\Controllers\CustomerController@addCustomers');
Route::put('customers/{id}', 'App\Http\Controllers\CustomerController@updateCustomers');
Route::delete('customers/{id}', 'App\Http\Controllers\CustomerController@deleteCustomers');
